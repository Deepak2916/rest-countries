import React from 'react';

const NotFound = () => {
  console.log('Error....');
  return (
    <div className='notFound'>
      <h1>404 - Page Not Found</h1>
      <p>Sorry, the page you are looking for does not exist.</p>
    </div>
  );
};

export default NotFound;