import React, { useState, useContext } from "react";
import { Link } from "react-router-dom";

import Card from "./components/card/Card";

import { Select } from "./components/select/Select.jsx";

import { Search } from "./components/search/Search";

import { darkModeContext } from "./App.jsx";

function RestCountries({ allCountries, isLoading }) {
  const [selectedSubRegion, setSelectedSubRegion] = useState("");
  const [selectedRegion, setSelectedRegion] = useState("");
  const [searchedCountry, setSearchedCountry] = useState("");
  const [sortby, setSortby] = useState([]);

  const [darkMode] = useContext(darkModeContext);

  const subregions = [];

  const onChangeSearchHandler = (country) => {
    setSearchedCountry(country.toLowerCase());
  };

  const selectHandlerForSubRegion = (subregions) => {
    if (subregions === "all subregions") {
      setSelectedSubRegion("");
    } else {
      setSelectedSubRegion(subregions);
    }
  };

  const selectHandlerForSorting = (sortby) => {
    if (sortby.includes("ascending")) {
      setSortby([...["ascending", sortby.split("(")[0]]]);
    } else {
      setSortby([...["descending", sortby.split("(")[0]]]);
    }
  };

  const selectHandlerForRegion = (region) => {
    setSelectedSubRegion("");
    if (region === "all regions") {
      setSelectedRegion("");
    } else {
      setSelectedRegion(region);
    }
  };

  let getFilteredCountries = allCountries.filter((country) => {
    if (
      country.subregion &&
      country.region.toLowerCase() === selectedRegion &&
      !subregions.includes(country.subregion)
    ) {
      subregions.push(country.subregion);
    }
    return (
      country.region.toLowerCase().includes(selectedRegion) &&
      ((country.subregion !== undefined &&
        country.subregion.toLowerCase().includes(selectedSubRegion)) ||
        true) &&
      country.name.common.toLowerCase().includes(searchedCountry)
    );
  });
  if (selectedSubRegion !== "") {
    getFilteredCountries = getFilteredCountries.filter((country) => {
      return country.subregion.toLowerCase() === selectedSubRegion;
    });
  }

  if (sortby.length) {
    if (sortby.includes("ascending")) {
      getFilteredCountries = getFilteredCountries.sort((country1, country2) => {
        return country1[sortby[1]] - country2[sortby[1]];
      });
    } else {
      getFilteredCountries = getFilteredCountries.sort((country1, country2) => {
        return country2[sortby[1]] - country1[sortby[1]];
      });
    }
  }

  return (
    <>
      <div
        className={
          (darkMode == "" && "main-container") || "main-container dark"
        }
      >
        <div className="inputs">
          <Search onChangeHandler={onChangeSearchHandler} />

          <>
            {selectedRegion !== "" && (
              <Select
                selectHandler={selectHandlerForSubRegion}
                title="filter by subregion"
                options={
                  (subregions.length && ["all subregions", ...subregions]) || []
                }
              />
            )}
            <Select
              selectHandler={selectHandlerForSorting}
              key={subregions[0]}
              title="sort by"
              options={[
                ["population(ascending)"],
                "population(descending)",
                "area(ascending)",
                "area(descending)",
              ]}
            />
          </>

          <Select
            selectHandler={selectHandlerForRegion}
            title="filter by region"
            options={[
              "all regions",
              "Africa",
              "Americas",
              "Asia",
              "Europe",
              "Oceania",
              "Antarctic",
            ]}
          />
        </div>
        <div className="cards-container ">
          {(isLoading && <div className="loader"></div>) ||
            (getFilteredCountries.length &&
              getFilteredCountries.map((country) => {
                return (
                  <Link
                    to={`/countryCode/${country.cca3}`}
                    key={country.name.common}
                  >
                    <Card
                      key={country.name.common}
                      details={{
                        countryName: country.name.common,
                        region: country.region,
                        population: country.population,
                        capital: country.capital,
                        img: country.flags.png,
                        subRegion: country.subregion,
                        area: country.area,
                      }}
                    />
                  </Link>
                );
              })) || (
              <>
                <h1>
                  Search Result: <span>not found</span>
                </h1>
              </>
            )}
        </div>
      </div>
    </>
  );
}

export default RestCountries;
