import React from "react";
import "./card.css";
function Card({ details }) {
  const { countryName, region, population, capital, img, subRegion, area } =
    details;
  return (
    <div className="card">
      <img src={img} alt={countryName} />
      <div className="country">
        <h3>{countryName}</h3>
        <div className="details">
          <h4>Population:{population}</h4>
          <h4>Region:{region}</h4>

          <h4>
            Capital:
            {(capital !== undefined && capital.join(",")) || "no capital"}
          </h4>

          <h4>subRegion:{subRegion}</h4>
          <h4>Area:{area}</h4>
        </div>
      </div>
    </div>
  );
}

export default Card;
