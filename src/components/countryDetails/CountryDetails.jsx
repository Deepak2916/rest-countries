import { Link, useParams,useNavigate} from "react-router-dom";
import { darkModeContext } from "../../App";

import React, { useContext } from "react";
import "./countryDetails.css";
export const CountryDetails = ({ countryCodes }) => {
  const { name } = useParams();
  const countryDetails = countryCodes[name];
  const [darkMode] = useContext(darkModeContext);

  const navigate = useNavigate()
  return (
    countryDetails !== undefined && (
      <div
        className={(darkMode && "outer-container dark") || "outer-container"}
      >
       
          <button 
            className={
              (darkMode && "btn  dark") || "btn "
            }
          onClick={
            ()=>navigate(-1)
          }>
            <span
            
            >
              <i className="fa-solid fa-arrow-left fa-lg"></i>Back
            </span>
          </button>
       
        <div className="country-details-container">
          <div className="flag">
            <img
              src={countryDetails.flags.svg}
              alt={countryDetails.name.common}
            />
          </div>
          <div className="details-container">
            <div className="country-name">
              <h1>{countryDetails.name.common}</h1>
            </div>
            <div className="column-container">
              <div className="columnOne">
                <span>
                  Native Name:
                  {countryDetails.name.nativeName &&
                    Object.values(countryDetails.name.nativeName)[0].common}
                </span>
                <span>Population:{countryDetails.population}</span>
                <span>Region:{countryDetails.region}</span>
                <span>Sub Region:{countryDetails.subregion}</span>
                <span>
                  Capital:
                  {(countryDetails.capital &&
                    countryDetails.capital.join(",")) ||
                    "no capital"}
                </span>
              </div>
              <div className="columnTwo">
                <span>
                  Top Level Domain:
                  {countryDetails.tld && countryDetails.tld.join(",")}
                </span>
                <span>
                  Currencies:
                  {countryDetails.currencies &&
                    Object.values(countryDetails.currencies)[0].name}
                </span>
                <span>
                  Languages:
                  {countryDetails.languages &&
                    Object.values(countryDetails.languages).join(",")}
                </span>
              </div>
            </div>
            <div className="border-contries-container">
              <span>Border Countries:</span>
              <div>
                {countryDetails.borders &&
                  countryDetails.borders.map((code) => {
                    return (
                      <Link key={code} to={`/countryCode/${code}`}>
                        <div>
                          <span className="botton-border">
                            {countryCodes[code].name.common}
                          </span>
                        </div>
                      </Link>
                    );
                  })}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  );
};
