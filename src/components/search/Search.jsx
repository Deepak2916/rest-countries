import React from "react";
import "./search.css";
export const Search = ({ onChangeHandler }) => {
  return (
    <div className="search">
      <input
        type="search"
        className="nosubmit"
        id="search"
        name="Search"
        onChange={(event) => {
          onChangeHandler(event.target.value || "");
        }}
        placeholder="Search..."
      />
    </div>
  );
};
