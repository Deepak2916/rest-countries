import React, { useContext } from "react";
import "./select.css";

import { darkModeContext } from "../../App";

export const Select = ({ selectHandler, options, title }) => {
  const [darkMode, setDarkMode] = useContext(darkModeContext);
  return (
    <select
      className={darkMode && "dark"}
      onChange={(event) => {
        if (event.target.value !== "") {
          selectHandler(event.target.value.toLocaleLowerCase());
        }
      }}
    >
      <option defaultValue={title} value="">
        {title}
      </option>
      {options.map((option) => {
        return (
          <option key={option} value={option}>
            {option}
          </option>
        );
      })}
    </select>
  );
};
