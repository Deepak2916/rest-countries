import React from "react";
import "./header.css";
import { darkModeContext } from "../../App";

import { useContext } from "react";
export const Header = () => {
  const [darkMode, setDarkMode] = useContext(darkModeContext);
  return (
    <div className={(darkMode == "" && "header") || "header dark"}>
      <h1>Where in the world?</h1>
      <button
        onClick={() => {
          if (darkMode == "") {
            setDarkMode("dark");
          } else {
            setDarkMode("");
          }
        }}
      >
        <i
          className="fa-regular fa-moon fa-2xl"
          style={(darkMode !== "" && { color: "#eeeff1" }) || {}}
        ></i>
        <h3 className={darkMode}>
          {(darkMode === "" && "Dark Mode") || "Light Mode"}
        </h3>
      </button>
    </div>
  );
};
