import { useEffect, useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Header } from "./components/header/Header";
import NotFound from "./NotFound";
import "./App.css";
import React from "react";
import RestCountries from "./RestCountries.jsx";
import { CountryDetails } from "./components/countryDetails/CountryDetails";

export const darkModeContext = React.createContext();

function App() {
  const [allCountries, setAllCountries] = useState([]);
  const [isLoding, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const [darkMode, setDarkMode] = useState("");

  useEffect(() => {
    async function fetchCountriesData() {
      try {
        const josonData = await fetch(`https://restcountries.com/v3.1/all`);

        const countriesData = await josonData.json();

        setAllCountries([...countriesData]);
      } catch (error) {
        setError(error.message);
      } finally {
        setIsLoading(false);
      }
    }
    fetchCountriesData();

    //data[0].region
    //data[0].population
    //data[0].capital
    //data[0].flags.png
    // data[0].name.common
    //data[0].subregion
    //data[0].area
    // console.log(region.path.split('/')[0]==='region');
  }, []);

  const countryCodes = allCountries.reduce((output, country) => {
    output[country.cca3] = country;
    // "cca3"
    return output;
  }, {});
  if (error) {
    return (
      <div>
        <h1>ERROR:{error}</h1>
      </div>
    );
  }

  return (
    <>
      <darkModeContext.Provider value={[darkMode, setDarkMode]}>
        

        <Router>
          <Routes>
            <Route
              path="/"
              element={
                <>
                <Header />
                <RestCountries
                  allCountries={allCountries}
                  isLoading={isLoding}
                />
                </>
              }
            />
            <Route
              path="/countryCode/:name"
              element={
                <>
                <Header />
                <CountryDetails
                  countryCodes={countryCodes}
                  isLoading={isLoding}
                />
                </>
              }
            />
            <Route path='*' element={<NotFound/>} /> 
          </Routes>
          
        </Router>
      </darkModeContext.Provider>
    </>
  );
}

export default App;
